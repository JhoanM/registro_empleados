import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TablaGeneralComponent } from './components/tabla-general/tabla-general.component';


const routes: Routes = [
  {
    path: 'tabla_general',
    component: TablaGeneralComponent
  },
  { path: '', redirectTo: 'tabla_general'},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistroEmpleadosRoutingModule { }
