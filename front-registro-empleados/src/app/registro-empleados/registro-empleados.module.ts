import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistroEmpleadosRoutingModule } from './registro-empleados-routing.module';
import { TablaGeneralComponent } from './components/tabla-general/tabla-general.component';
import { RegistroEmpleadoComponent } from './components/registro-empleado/registro-empleado.component';


@NgModule({
  declarations: [TablaGeneralComponent, RegistroEmpleadoComponent],
  imports: [
    CommonModule,
    RegistroEmpleadosRoutingModule
  ]
})
export class RegistroEmpleadosModule { }
