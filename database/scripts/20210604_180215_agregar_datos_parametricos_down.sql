DELETE FROM registro_empleados.pais
    WHERE abreviatura IN
        ('COL','EEUU');

DELETE FROM registro_empleados.tipo_documento
    WHERE abreviatura IN
        ('CC','CE','PP','PE');

DELETE FROM registro_empleados.area
    WHERE abreviatura IN
        ('ADM','FN','CP','IE','OP','SV');     