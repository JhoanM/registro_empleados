-- object: registro_empleados | type: SCHEMA --
-- DROP SCHEMA IF EXISTS registro_empleados CASCADE;
CREATE SCHEMA IF NOT EXISTS registro_empleados;
-- ddl-end --
-- ALTER SCHEMA registro_empleados OWNER TO postgres;
-- ddl-end --

SET search_path TO pg_catalog,public,registro_empleados;
-- ddl-end --

-- object: registro_empleados.empleado | type: TABLE --
-- DROP TABLE IF EXISTS registro_empleados.empleado CASCADE;
CREATE TABLE registro_empleados.empleado (
	id serial NOT NULL,
	primer_apellido varchar(20),
	segundo_apellido varchar(20),
	primer_nombre varchar(20),
	otros_nombres varchar(50),
	numero_identificacion integer,
	correo_electronico varchar(300),
	fecha_ingreso timestamp,
	fecha_creacion timestamp,
	fecha_modificacion timestamp,
	pais_id integer NOT NULL,
	tipo_documento_id integer NOT NULL,
	area_id integer NOT NULL,
	CONSTRAINT empleado_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE registro_empleados.empleado OWNER TO postgres;
-- ddl-end --

-- object: registro_empleados.pais | type: TABLE --
-- DROP TABLE IF EXISTS registro_empleados.pais CASCADE;
CREATE TABLE registro_empleados.pais (
	id serial NOT NULL,
	nombre varchar,
	abreviatura varchar(10),
	fecha_creacion timestamp,
	fecha_modificacion timestamp,
	CONSTRAINT pais_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE registro_empleados.pais OWNER TO postgres;
-- ddl-end --

-- object: registro_empleados.tipo_documento | type: TABLE --
-- DROP TABLE IF EXISTS registro_empleados.tipo_documento CASCADE;
CREATE TABLE registro_empleados.tipo_documento (
	id serial NOT NULL,
	nombre varchar,
	abreviatura varchar(10),
	fecha_creacion timestamp,
	fecha_modificacion timestamp,
	CONSTRAINT tipo_documento_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE registro_empleados.tipo_documento OWNER TO postgres;
-- ddl-end --

-- object: registro_empleados.area | type: TABLE --
-- DROP TABLE IF EXISTS registro_empleados.area CASCADE;
CREATE TABLE registro_empleados.area (
	id serial NOT NULL,
	nombre varchar,
	abreviatura varchar(10),
	fecha_creacion timestamp,
	fecha_modificacion timestamp,
	CONSTRAINT area_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE registro_empleados.area OWNER TO postgres;
-- ddl-end --

-- object: pais_fk | type: CONSTRAINT --
-- ALTER TABLE registro_empleados.empleado DROP CONSTRAINT IF EXISTS pais_fk CASCADE;
ALTER TABLE registro_empleados.empleado ADD CONSTRAINT pais_fk FOREIGN KEY (pais_id)
REFERENCES registro_empleados.pais (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: tipo_documento_fk | type: CONSTRAINT --
-- ALTER TABLE registro_empleados.empleado DROP CONSTRAINT IF EXISTS tipo_documento_fk CASCADE;
ALTER TABLE registro_empleados.empleado ADD CONSTRAINT tipo_documento_fk FOREIGN KEY (tipo_documento_id)
REFERENCES registro_empleados.tipo_documento (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: area_fk | type: CONSTRAINT --
-- ALTER TABLE registro_empleados.empleado DROP CONSTRAINT IF EXISTS area_fk CASCADE;
ALTER TABLE registro_empleados.empleado ADD CONSTRAINT area_fk FOREIGN KEY (area_id)
REFERENCES registro_empleados.area (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --
