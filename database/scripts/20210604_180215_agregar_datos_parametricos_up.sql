INSERT INTO registro_empleados.pais (nombre,
    abreviatura,
    fecha_creacion,
    fecha_modificacion)
VALUES
('Colombia', 'COL', NULL, NULL),
('Estados Unidos', 'EEUU', NULL, NULL);

INSERT INTO registro_empleados.tipo_documento (nombre,
    abreviatura,
    fecha_creacion,
    fecha_modificacion)
VALUES
('Cedula de Ciudadania', 'CC', NULL, NULL),
('Cedula de Extranjeria', 'CE', NULL, NULL),
('Pasaporte', 'PP', NULL, NULL),
('Permiso Especial', 'PE', NULL, NULL);

INSERT INTO registro_empleados.area (nombre,
    abreviatura,
    fecha_creacion,
    fecha_modificacion)
VALUES
('Administracion', 'ADM', NULL, NULL),
('Financiera', 'FN', NULL, NULL),
('Compras', 'CP', NULL, NULL),
('Infraestructura', 'IE', NULL, NULL),
('Operacion', 'OP', NULL, NULL),
('Servicios Varios', 'SV', NULL, NULL);