package main

import (
	"fmt"
	"io/ioutil"
	"strings"
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CrearEsquemaRegistroEmpleados_20210603_235731 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CrearEsquemaRegistroEmpleados_20210603_235731{}
	m.Created = "20210603_235731"

	migration.Register("CrearEsquemaRegistroEmpleados_20210603_235731", m)
}

// Run the migrations
func (m *CrearEsquemaRegistroEmpleados_20210603_235731) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	file, err := ioutil.ReadFile("../scripts/20210603_235731_crear_esquema_registro_empleados_up.sql")

	if err != nil {
		// handle error
		fmt.Println(err)
	}

	requests := strings.Split(string(file), ";")

	for _, request := range requests {
		fmt.Println(request)
		m.SQL(request)
		// do whatever you need with result and error
	}

}

// Reverse the migrations
func (m *CrearEsquemaRegistroEmpleados_20210603_235731) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	file, err := ioutil.ReadFile("../scripts/20210603_235731_crear_esquema_registro_empleados_down.sql")

	if err != nil {
		// handle error
		fmt.Println(err)
	}

	requests := strings.Split(string(file), ";")

	for _, request := range requests {
		fmt.Println(request)
		m.SQL(request)
		// do whatever you need with result and error
	}

}
