package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"],
        beego.ControllerComments{
            Method: "Post",
            Router: "/",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: "/",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: "/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"],
        beego.ControllerComments{
            Method: "Put",
            Router: "/:id",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:AreaController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/:id",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"],
        beego.ControllerComments{
            Method: "Post",
            Router: "/",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: "/",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: "/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"],
        beego.ControllerComments{
            Method: "Put",
            Router: "/:id",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:EmpleadoController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/:id",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"],
        beego.ControllerComments{
            Method: "Post",
            Router: "/",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: "/",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: "/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"],
        beego.ControllerComments{
            Method: "Put",
            Router: "/:id",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:PaisController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/:id",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"],
        beego.ControllerComments{
            Method: "Post",
            Router: "/",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: "/",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: "/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"],
        beego.ControllerComments{
            Method: "Put",
            Router: "/:id",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"] = append(beego.GlobalControllerRouter["gitlab.com/JhoanM/registro_empleados/controllers:TipoDocumentoController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/:id",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
